#version 330

uniform samplerCube cubeMap;					
in vec2 outUV;								

uniform mat3 FNmatrix;

uniform mat3 RPmatrix;
uniform mat3 RNmatrix;

uniform mat3 UPmatrix;
uniform mat3 UNmatrix;

layout(location = 0) out vec4 FP;
layout(location = 1) out vec4 FN;
layout(location = 2) out vec4 RP;
layout(location = 3) out vec4 RN;
layout(location = 4) out vec4 UP;
layout(location = 5) out vec4 UN;

void main()									
{											
	const float inc = 2.0f / 16.0f;

	vec3 FNcolor = vec3(0.0f), FPcolor = vec3(0.0f);
	vec3 RNcolor = vec3(0.0f), RPcolor = vec3(0.0f);
	vec3 UNcolor = vec3(0.0f), UPcolor = vec3(0.0f);

	vec3 normal = vec3(outUV.x, -outUV.y, 1.0f);
	normal = normal * 2.0f - 1.0f;
	normal = normalize(normal);

	float dotSum = 0.0f;

	for (float x = -1.0f; x <= 1.0f; x += inc)
	{
		for (float y = -1.0f; y <= 1.0f; y += inc)
		{
			vec3 rayOffset = vec3(x * 0.5f, y * 0.5f, 0.0f);
			vec3 ray = normal + rayOffset;
			ray = normalize(ray);

			float NdotR = max(0.0f, dot(ray, normal));
			dotSum += NdotR;

			FPcolor += texture(cubeMap, ray).rgb * NdotR;
			FNcolor += texture(cubeMap, FNmatrix * ray).rgb * NdotR;

			RPcolor += texture(cubeMap, RPmatrix * ray).rgb * NdotR;
			RNcolor += texture(cubeMap, RNmatrix * ray).rgb * NdotR;

			UPcolor += texture(cubeMap, UPmatrix * ray).rgb * NdotR;
			UNcolor += texture(cubeMap, UNmatrix * ray).rgb * NdotR;
		}
	}

	FPcolor /= dotSum;
	FNcolor /= dotSum;
			   dotSum;
	RPcolor /= dotSum;
	RNcolor /= dotSum;
			   dotSum;
	UPcolor /= dotSum;
	UNcolor /= dotSum;

	FN = vec4(FNcolor.xyz, 1.0f);
	FP = vec4(FPcolor.xyz, 1.0f);
	RN = vec4(RNcolor.xyz, 1.0f);
	RP = vec4(RPcolor.xyz, 1.0f);
	UN = vec4(UNcolor.xyz, 1.0f);
	UP = vec4(UPcolor.xyz, 1.0f);
}											