#include "Global.h"

#pragma region Variables
const string shader_v_src = "shader_v.glsl";
const string shader_f_src = "shader_f";

const int FP = 0;
const int FN = 1;
const int RP = 2;
const int RN = 3;
const int UP = 4;
const int UN = 5;
const int I = 6;

int outputEnvSize;

string inputEnvMap;
string outputEnvMap;
GLuint generateProgram;
GLuint input_cube_map;
GLuint output_cube_map[7];
GLuint framebuffer;

GLfloat vertices[] =
{
	-1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	-1.0f, -1.0f, 0.0f
};
GLfloat uvs[] =
{
	0.0f, 0.0f,
	1.0f, 0.0f,
	1.0f, -1.0f,
	0.0f, -1.0f
};
GLuint verticesBuffer;
GLuint uvsBuffer;

SDL_Window* glWindow = nullptr;
SDL_GLContext glContext;

char irradianceType;
#pragma endregion

#pragma region Methods
bool Init()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		return false;
	}
	//Use OpenGL 3.1 core
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	glWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, outputEnvSize, outputEnvSize, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (glWindow == NULL)
	{
		printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
		return false;
	}
	//Create context
	glContext = SDL_GL_CreateContext(glWindow);
	if (glContext == NULL)
	{
		printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
		return false;
	}

	//Create window
	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();
	if (glewError != GLEW_OK)
	{
		printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
		return false;
	}

	//Use Vsync
	if (SDL_GL_SetSwapInterval(1) < 0)
	{
		printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
	}

	//Initialize OpenGL
	if (!InitGL())
	{
		printf("Unable to initialize OpenGL!\n");
		return false;
	}
	//Init SDL_image
	if (IMG_Init(IMG_INIT_PNG) == 0)
	{
		printf("Unable to initialize IMG!\n");
		return false;
	}

	return true;
}
void LoadShader(const string& src, const GLenum& shader_type)
{
	ifstream shader_src(src.data());

	string shader_code((istreambuf_iterator<char>(shader_src)), (istreambuf_iterator<char>()));

	shader_src.close();

	const char* c_str = shader_code.c_str();

	GLuint shdr = glCreateShader(shader_type);
	glShaderSource(shdr, 1, &c_str, NULL);

	glCompileShader(shdr);

	GLint status;
	glGetShaderiv(shdr, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shdr, GL_INFO_LOG_LENGTH, &maxLength);
		GLchar* log = new GLchar[maxLength];

		glGetShaderInfoLog(shdr, maxLength, &maxLength, log);
		cout << endl << log << endl;
		delete[] log;
	}
	glAttachShader(generateProgram, shdr);
}
bool InitGL()
{
	/////////	SHADERS FOR SCENE PLANE		/////
	//Create program for shaders
	generateProgram = glCreateProgram();
	if (!glIsProgram(generateProgram))
	{
		cout << "program\n";
		return false;
	}
	GLint status;

	LoadShader(shader_v_src, GL_VERTEX_SHADER);
	LoadShader(shader_f_src + '_' + irradianceType + ".glsl", GL_FRAGMENT_SHADER);

	glLinkProgram(generateProgram);

	//Validate linking
	glGetProgramiv(generateProgram, GL_LINK_STATUS, &status);
	if (status != GL_TRUE)
	{
		cout << "link\n";
		return false;
	}

	glGenBuffers(1, &verticesBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glGenBuffers(1, &uvsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvsBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_STATIC_DRAW);

	glGenTextures(1, &input_cube_map);
	glBindTexture(GL_TEXTURE_CUBE_MAP, input_cube_map);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	SDL_Surface* cubeFace = IMG_Load((inputEnvMap + "FP.png").c_str()); //front positive
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, cubeFace->w, cubeFace->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, cubeFace->pixels);
	SDL_FreeSurface(cubeFace);
	cubeFace = IMG_Load((inputEnvMap + "FN.png").c_str()); //front negative
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, cubeFace->w, cubeFace->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, cubeFace->pixels);
	SDL_FreeSurface(cubeFace);
	cubeFace = IMG_Load((inputEnvMap + "RP.png").c_str()); //right positive
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, cubeFace->w, cubeFace->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, cubeFace->pixels);
	SDL_FreeSurface(cubeFace);
	cubeFace = IMG_Load((inputEnvMap + "RN.png").c_str()); //right negative
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, cubeFace->w, cubeFace->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, cubeFace->pixels);
	SDL_FreeSurface(cubeFace);
	cubeFace = IMG_Load((inputEnvMap + "UP.png").c_str()); //up positive
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, cubeFace->w, cubeFace->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, cubeFace->pixels);
	SDL_FreeSurface(cubeFace);
	cubeFace = IMG_Load((inputEnvMap + "UN.png").c_str()); //up negative
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, cubeFace->w, cubeFace->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, cubeFace->pixels);
	SDL_FreeSurface(cubeFace);

	glGenTextures(7, output_cube_map);

	glBindTexture(GL_TEXTURE_2D, output_cube_map[FP]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, outputEnvSize, outputEnvSize, 0, GL_RGBA, GL_FLOAT, NULL);
	glBindTexture(GL_TEXTURE_2D, output_cube_map[FN]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, outputEnvSize, outputEnvSize, 0, GL_RGBA, GL_FLOAT, NULL);

	glBindTexture(GL_TEXTURE_2D, output_cube_map[RP]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, outputEnvSize, outputEnvSize, 0, GL_RGBA, GL_FLOAT, NULL);
	glBindTexture(GL_TEXTURE_2D, output_cube_map[RN]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, outputEnvSize, outputEnvSize, 0, GL_RGBA, GL_FLOAT, NULL);

	glBindTexture(GL_TEXTURE_2D, output_cube_map[UP]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, outputEnvSize, outputEnvSize, 0, GL_RGBA, GL_FLOAT, NULL);
	glBindTexture(GL_TEXTURE_2D, output_cube_map[UN]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, outputEnvSize, outputEnvSize, 0, GL_RGBA, GL_FLOAT, NULL);

	glBindTexture(GL_TEXTURE_2D, output_cube_map[I]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, outputEnvSize, outputEnvSize, 0, GL_RGBA, GL_FLOAT, NULL);

	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, output_cube_map[FP], 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, output_cube_map[FN], 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, output_cube_map[RP], 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, output_cube_map[RN], 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, output_cube_map[UP], 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, output_cube_map[UN], 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT6, GL_TEXTURE_2D, output_cube_map[I], 0);

	GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6 };
	glDrawBuffers(7, buffers);

	glDisable(GL_DEPTH);

	return true;
}
void GenerateDiffuse()
{
	const GLfloat FNMatrix[] = { 
		-1.f, 0.f, 0.f,
		0.f, 1.f, 0.f,
		0.f, 0.f, -1.f 
	};

	const GLfloat RPMatrix[] = { 
		0.f, 0.f, -1.f,
		0.f, 1.f, 0.f,
		1.f, 0.f, 0.f 
	};
	const GLfloat RNMatrix[] = { 
		0.f, 0.f, 1.f,
		0.f, 1.f, 0.f,
		-1.f, 0.f, 0.f 
	};

	const GLfloat UPMatrix[] = { 
		1.f, 0.f, 0.f,
		0.f, 0.f, -1.f,
		0.f, 1.f, 0.f 
	};
	const GLfloat UNMatrix[] = { 
		1.f, 0.f, 0.f,
		0.f, 0.f, 1.f,
		0.f, -1.f, 0.f 
	};


	glUseProgram(generateProgram);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	glUniformMatrix3fv(glGetUniformLocation(generateProgram, "FNmatrix"), 1, GL_FALSE, FNMatrix);

	glUniformMatrix3fv(glGetUniformLocation(generateProgram, "RPmatrix"), 1, GL_FALSE, RPMatrix);
	glUniformMatrix3fv(glGetUniformLocation(generateProgram, "RNmatrix"), 1, GL_FALSE, RNMatrix);

	glUniformMatrix3fv(glGetUniformLocation(generateProgram, "UPmatrix"), 1, GL_FALSE, UPMatrix);
	glUniformMatrix3fv(glGetUniformLocation(generateProgram, "UNmatrix"), 1, GL_FALSE, UNMatrix);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvsBuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, input_cube_map);
	glUniform1i(glGetUniformLocation(generateProgram, "cubeMap"), 0);

	glDrawArrays(GL_QUADS, 0, 4);

	Save();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
void Save()
{
	const string FP = "outMapDiff/" + outputEnvMap + "FP" + '_' + irradianceType + ".png";
	const string FN = "outMapDiff/" + outputEnvMap + "FN" + '_' + irradianceType + ".png";
										  
	const string RP = "outMapDiff/" + outputEnvMap + "RP" + '_' + irradianceType + ".png";
	const string RN = "outMapDiff/" + outputEnvMap + "RN" + '_' + irradianceType + ".png";
									  
	const string UP = "outMapDiff/" + outputEnvMap + "UP" + '_' + irradianceType + ".png";
	const string UN = "outMapDiff/" + outputEnvMap + "UN" + '_' + irradianceType + ".png";

	cout << "Save process\n";
	Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif

	unsigned char* pixels = new unsigned char[outputEnvSize * outputEnvSize * 4];
	SDL_Surface* save = SDL_CreateRGBSurface(0, outputEnvSize, outputEnvSize, 32, rmask, gmask, bmask, amask);
	///////// FRONT
	glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process1\n";
	IMG_SavePNG(save, FP.c_str());
	glReadBuffer(GL_COLOR_ATTACHMENT1);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process2\n";
	IMG_SavePNG(save, FN.c_str());

	glReadBuffer(GL_COLOR_ATTACHMENT2);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process1\n";
	IMG_SavePNG(save, RP.c_str());
	glReadBuffer(GL_COLOR_ATTACHMENT3);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process2\n";
	IMG_SavePNG(save, RN.c_str());

	glReadBuffer(GL_COLOR_ATTACHMENT4);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process1\n";
	IMG_SavePNG(save, UP.c_str());
	glReadBuffer(GL_COLOR_ATTACHMENT5);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process2\n";
	IMG_SavePNG(save, UN.c_str());


	SDL_FreeSurface(save);
	delete[] pixels;
	cout << "Save process end\n";

}
void Save(int roughness)
{
	const string FP = "outMapSpec/" + to_string(roughness) + outputEnvMap + "FP" + '_' + irradianceType + ".png";
	const string FN = "outMapSpec/" + to_string(roughness) + outputEnvMap + "FN" + '_' + irradianceType + ".png";
							   
	const string RP = "outMapSpec/" + to_string(roughness) + outputEnvMap + "RP" + '_' + irradianceType + ".png";
	const string RN = "outMapSpec/" + to_string(roughness) + outputEnvMap + "RN" + '_' + irradianceType + ".png";
							   
	const string UP = "outMapSpec/" + to_string(roughness) + outputEnvMap + "UP" + '_' + irradianceType + ".png";
	const string UN = "outMapSpec/" + to_string(roughness) + outputEnvMap + "UN" + '_' + irradianceType + ".png";

	cout << "Save process\n";
	Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif

	unsigned char* pixels = new unsigned char[outputEnvSize * outputEnvSize * 4];
	SDL_Surface* save = SDL_CreateRGBSurface(0, outputEnvSize, outputEnvSize, 32, rmask, gmask, bmask, amask);
	///////// FRONT
	glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process1\n";
	IMG_SavePNG(save, FP.c_str());
	glReadBuffer(GL_COLOR_ATTACHMENT1);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process2\n";
	IMG_SavePNG(save, FN.c_str());

	glReadBuffer(GL_COLOR_ATTACHMENT2);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process1\n";
	IMG_SavePNG(save, RP.c_str());
	glReadBuffer(GL_COLOR_ATTACHMENT3);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process2\n";
	IMG_SavePNG(save, RN.c_str());

	glReadBuffer(GL_COLOR_ATTACHMENT4);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process1\n";
	IMG_SavePNG(save, UP.c_str());
	glReadBuffer(GL_COLOR_ATTACHMENT5);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process2\n";
	IMG_SavePNG(save, UN.c_str());


	SDL_FreeSurface(save);
	delete[] pixels;
	cout << "Save process end\n";

}
void SaveINT()
{
	const string INTs = "outMapInt/int.png";

	cout << "Save process\n";
	Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif

	unsigned char* pixels = new unsigned char[outputEnvSize * outputEnvSize * 4];
	SDL_Surface* save = SDL_CreateRGBSurface(0, outputEnvSize, outputEnvSize, 32, rmask, gmask, bmask, amask);
	///////// FRONT
	glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer);
	glReadBuffer(GL_COLOR_ATTACHMENT6);
	glReadPixels(0, 0, outputEnvSize, outputEnvSize, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	save->pixels = pixels;
	cout << "Save process1\n";
	IMG_SavePNG(save, INTs.c_str());

	SDL_FreeSurface(save);
	delete[] pixels;
	cout << "Save process end\n";

}
void GenerateSpecular()
{
	glUseProgram(generateProgram);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvsBuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, input_cube_map);
	glUniform1i(glGetUniformLocation(generateProgram, "cubeMap"), 0);

	for (float i = 0; i <= 6; ++i)
	{
		glUniform1f(glGetUniformLocation(generateProgram, "roughness"), i * (1.0f / 6.0f));
		glDrawArrays(GL_QUADS, 0, 4);
		Save(i);
	}
	SaveINT();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
void Close()
{
	cout << "Cleanup process\n";
	glDeleteProgram(generateProgram);
	cout << "Cleanup after program\n";
	glDeleteFramebuffers(1, &framebuffer);
	cout << "Cleanup after framebuffer\n";
	glDeleteBuffers(1, &verticesBuffer);
	glDeleteBuffers(1, &uvsBuffer);
	cout << "Cleanup after buffers\n";
	glDeleteTextures(1, &input_cube_map);
	glDeleteTextures(7, output_cube_map);
	cout << "Cleanup afeter cubemaps\n";

	glUseProgram(0);
	SDL_DestroyWindow(glWindow);
	glWindow = NULL;
	cout << "Cleanup afeter widnows\n";
	SDL_Quit();
	cout << "Cleanup end\n";
}
#pragma endregion
