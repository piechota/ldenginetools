#version 330

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec2 inUV;

out vec2 outUV;

void main()
{
	gl_Position = inPosition;
	outUV = inUV;
}