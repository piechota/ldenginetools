#pragma once

#include "Headers.h"

#pragma region Variables
extern int outputEnvSize;

extern string inputEnvMap;
extern string outputEnvMap;
extern GLuint generateProgram;
extern GLuint input_cube_map;
extern GLuint framebuffer;

extern char irradianceType;
#pragma endregion


#pragma region Methods
extern bool Init();
extern bool InitGL();
extern void LoadShader(const string& src, const GLenum& shader_type);
extern void GenerateDiffuse();
extern void GenerateSpecular();
extern void Save();
extern void Close();
#pragma endregion
