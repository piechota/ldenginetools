#version 330

uniform samplerCube cubeMap;
uniform float roughness;
in vec2 outUV;								

layout(location = 0) out vec4 FP;
layout(location = 1) out vec4 FN;
layout(location = 2) out vec4 RP;
layout(location = 3) out vec4 RN;
layout(location = 4) out vec4 UP;
layout(location = 5) out vec4 UN;
layout(location = 6) out vec4 INT;

mat3 FNmatrix = mat3(-1.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f,
					0.0f, 0.0f, -1.0f);

mat3 RPmatrix = mat3(0.0f, 0.0f, -1.0f,
					0.0f, 1.0f, 0.0f,
					1.0f, 0.0f, 0.0f);
mat3 RNmatrix = mat3(0.0f, 0.0f, 1.0f,
					0.0f, 1.0f, 0.0f,
					-1.0f, 0.0f, 0.0f);

mat3 UPmatrix = mat3(1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, -1.0f,
					0.0f, 1.0f, 0.0f);
mat3 UNmatrix = mat3(1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 1.0f,
					0.0f, -1.0f, 0.0f);

const float PI = 3.14159265358979f;

float RadicalInverse_VdC(uint bits)
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);

	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}
vec2 Hammersley(uint i, uint n)
{
	return vec2(float(i) / float(n), RadicalInverse_VdC(i));
}
vec3 ImportanceSampleGGX(vec2 xi, float roughness, vec3 n)
{
	float a = roughness * roughness;
	float phi = 2 * PI * xi.x;
	float cosTheta = sqrt((1.0f - xi.y) / (1.0f + (a * a - 1.0f) * xi.y));
	float sinTheta = sqrt(1.0f - cosTheta * cosTheta);

	vec3 h;
	h.x = sinTheta * cos(phi);
	h.y = sinTheta * sin(phi);
	h.z = cosTheta;

	vec3 upVector = abs(n.z) < 0.999f ? vec3(0.0f, 0.0f, 1.0f) : vec3(1.0f, 0.0f, 0.0f);
	vec3 tangentX = normalize(cross(upVector, n));
	vec3 tangentY = cross(n, tangentX);

	return tangentX * h.x + tangentY * h.y + n * h.z;
}
vec3 PrefilterEnvMap(float roughness, vec3 r)
{
	vec3 n = r;
	vec3 v = r;

	vec3 prefilteredColor = vec3(0.0f);
	float totalWeight = 0.0f;

	const uint numSamples = 1024u;
	for (uint i = 0u; i < numSamples; ++i)
	{
		vec2 xi = Hammersley(i, numSamples);
		vec3 h = ImportanceSampleGGX(xi, roughness, n);
		vec3 l = 2 * dot(v, h) * h - v;
		
		float NoL = max(0.0f, dot(n, l));
		if (NoL > 0.0f)
		{
			prefilteredColor += texture(cubeMap, l).rgb * NoL;
			totalWeight += NoL;
		}
	}

	return prefilteredColor / totalWeight;
}

float G_Smith(float roughness, float NoV, float NoL)
{
	float k = roughness * roughness  * 0.5f;

	float gl = NoL / (NoL * (1.0f - k) + k);
	float gv = NoV / (NoV * (1.0f - k) + k);

	return gl * gv;
}

vec2 IntegrateBRDF(float roughness, float NoV)
{
	vec3 v;
	v.x = sqrt(1.0f - NoV * NoV);
	v.y = 0.0f;
	v.z = NoV;

	float a = 0.0f;
	float b = 0.0f;

	const uint numSamples = 1024u;
	for (uint i = 0u; i < numSamples; ++i)
	{
		vec2 xi = Hammersley(i, numSamples);
		vec3 h = ImportanceSampleGGX(xi, roughness, vec3(0.0f, 0.0f, 1.0f));
		vec3 l = 2 * dot(v, h) * h - v;

		float NoL = max(0.0f, l.z);
		float NoH = max(0.0f, h.z);
		float VoH = max(0.0f, dot(v, h));

		if (NoL > 0)
		{
			float g = G_Smith(roughness, NoV, NoL);

			float gVis = g * VoH / (NoH * NoV);
			float fc = pow(1.0f - VoH, 5);
			a += (1.0f - fc) * gVis;
			b += fc * gVis;
		}
	}

	return vec2(a, b) / numSamples;
}

void main()									
{											
	const float inc = 2.0f / 16.0f;

	vec3 FNcolor = vec3(0.0f), FPcolor = vec3(0.0f);
	vec3 RNcolor = vec3(0.0f), RPcolor = vec3(0.0f);
	vec3 UNcolor = vec3(0.0f), UPcolor = vec3(0.0f);

	vec4 INTcolor = vec4(0.0f);

	vec3 normal = vec3(outUV.x, -outUV.y, 1.0f);
	normal = normal * 2.0f - 1.0f;
	normal = normalize(normal);

	FPcolor = PrefilterEnvMap(roughness, normal);
	FNcolor = PrefilterEnvMap(roughness, FNmatrix * normal);
							  
	RPcolor = PrefilterEnvMap(roughness, RPmatrix * normal);
	RNcolor = PrefilterEnvMap(roughness, RNmatrix * normal);
							  
	UPcolor = PrefilterEnvMap(roughness, UPmatrix * normal);
	UNcolor = PrefilterEnvMap(roughness, UNmatrix * normal);

	INTcolor.xy = IntegrateBRDF(outUV.y, outUV.x);
	INTcolor.w = 1.0f;

	FN = vec4(FNcolor.xyz, 1.0f);
	FP = vec4(FPcolor.xyz, 1.0f);
	RN = vec4(RNcolor.xyz, 1.0f);
	RP = vec4(RPcolor.xyz, 1.0f);
	UN = vec4(UNcolor.xyz, 1.0f);
	UP = vec4(UPcolor.xyz, 1.0f);
	INT = INTcolor;
}											