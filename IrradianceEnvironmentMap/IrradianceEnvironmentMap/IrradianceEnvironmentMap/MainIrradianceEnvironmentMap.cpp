#include "Global.h"

int main(int argc, char* args[])
{
	if (argc > 1)
	{
		inputEnvMap = args[1];
		outputEnvMap = args[2];
		outputEnvSize = stoi(args[3]);
		irradianceType = args[4][0];
	}
	else
	{
		cout << "Use console!\n";
		getchar();
		return 0;
	}

	if (!Init())
		return 0;

	if (irradianceType == 'd')
	{
		GenerateDiffuse();
	}
	else
	{
		GenerateSpecular();
	}

	Close();

	return 0;
}