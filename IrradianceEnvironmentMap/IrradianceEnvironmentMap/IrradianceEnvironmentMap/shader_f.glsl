#version 330

uniform samplerCube cubeMap;					
in vec2 outUV;								

uniform mat3 FNmatrix;

uniform mat3 RPmatrix;
uniform mat3 RNmatrix;

uniform mat3 UPmatrix;
uniform mat3 UNmatrix;

layout(location = 0) out vec4 FP;
layout(location = 1) out vec4 FN;
layout(location = 2) out vec4 RP;
layout(location = 3) out vec4 RN;
layout(location = 4) out vec4 UP;
layout(location = 5) out vec4 UN;

vec4 sampleColor(vec3 normal, vec3 ray)
{
	vec4 result;

	float NdotR = max(0.0f, dot(normal, ray));
	result.xyz = texture(cubeMap, ray).rgb * NdotR;
	result.w = NdotR;

	return result;
}
void main()									
{											
	const float inc = 0.01f;

	vec4 FNcolor = vec4(0.0f), FPcolor = vec4(0.0f);
	vec4 RNcolor = vec4(0.0f), RPcolor = vec4(0.0f);
	vec4 UNcolor = vec4(0.0f), UPcolor = vec4(0.0f);

	vec3 normal = vec3(outUV.x, -outUV.y, 1.0f);
	normal = normal * 2.0f - 1.0f;
	normal = normalize(normal);

	for (float x = -1.0f; x <= 1.0f; x += inc)
	{
		for (float y = -1.0f; y <= 1.0f; y += inc)
		{
			vec3 ray = vec3(x, y, 1.0f);
			ray = normalize(ray);

			vec3 FNray = FNmatrix * ray;
			vec3 RPray = RPmatrix * ray;
			vec3 RNray = RNmatrix * ray;
			vec3 UPray = UPmatrix * ray;
			vec3 UNray = UNmatrix * ray;

			vec3 FNnormal = FNmatrix * normal;
			vec3 RPnormal = RPmatrix * normal;
			vec3 RNnormal = RNmatrix * normal;
			vec3 UPnormal = UPmatrix * normal;
			vec3 UNnormal = UNmatrix * normal;
			/////
			FPcolor += sampleColor(normal, ray);
			FPcolor += sampleColor(normal, FNray);
			FPcolor += sampleColor(normal, RPray);
			FPcolor += sampleColor(normal, RNray);
			FPcolor += sampleColor(normal, UPray);
			FPcolor += sampleColor(normal, UNray);

			FNcolor += sampleColor(FNnormal, ray);
			FNcolor += sampleColor(FNnormal, FNray);
			FNcolor += sampleColor(FNnormal, RPray);
			FNcolor += sampleColor(FNnormal, RNray);
			FNcolor += sampleColor(FNnormal, UPray);
			FNcolor += sampleColor(FNnormal, UNray);
			////////
			RPcolor += sampleColor(RPnormal, ray);
			RPcolor += sampleColor(RPnormal, FNray);
			RPcolor += sampleColor(RPnormal, RPray);
			RPcolor += sampleColor(RPnormal, RNray);
			RPcolor += sampleColor(RPnormal, UPray);
			RPcolor += sampleColor(RPnormal, UNray);

			RNcolor += sampleColor(RNnormal, ray);
			RNcolor += sampleColor(RNnormal, FNray);
			RNcolor += sampleColor(RNnormal, RPray);
			RNcolor += sampleColor(RNnormal, RNray);
			RNcolor += sampleColor(RNnormal, UPray);
			RNcolor += sampleColor(RNnormal, UNray);
			///////
			UPcolor += sampleColor(UPnormal, ray);
			UPcolor += sampleColor(UPnormal, FNray);
			UPcolor += sampleColor(UPnormal, RPray);
			UPcolor += sampleColor(UPnormal, RNray);
			UPcolor += sampleColor(UPnormal, UPray);
			UPcolor += sampleColor(UPnormal, UNray);

			UNcolor += sampleColor(UNnormal, ray);
			UNcolor += sampleColor(UNnormal, FNray);
			UNcolor += sampleColor(UNnormal, RPray);
			UNcolor += sampleColor(UNnormal, RNray);
			UNcolor += sampleColor(UNnormal, UPray);
			UNcolor += sampleColor(UNnormal, UNray);
		}
	}

	FPcolor.xyz /= FPcolor.w;
	FNcolor.xyz /= FNcolor.w;

	RPcolor.xyz /= RPcolor.w;
	RNcolor.xyz /= RNcolor.w;

	UPcolor.xyz /= UPcolor.w;
	UNcolor.xyz /= UNcolor.w;

	FN = vec4(FNcolor.xyz, 1.0f);
	FP = vec4(FPcolor.xyz, 1.0f);
	RN = vec4(RNcolor.xyz, 1.0f);
	RP = vec4(RPcolor.xyz, 1.0f);
	UN = vec4(UNcolor.xyz, 1.0f);
	UP = vec4(UPcolor.xyz, 1.0f);
}											